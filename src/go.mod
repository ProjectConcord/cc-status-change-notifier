module gitlab.com/ProjectConcord/cc-status-change-notifier

go 1.15

require (
	github.com/bitwurx/jrpc2 v0.0.0-20200508153510-d8310ad1baf0
	github.com/gofrs/uuid v3.3.0+incompatible
	github.com/gorilla/websocket v1.4.2
)
